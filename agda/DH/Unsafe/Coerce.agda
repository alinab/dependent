module DH.Unsafe.Coerce where

open import Level

postulate
  unsafeCoerce : ∀ {ℓ₁ ℓ₂ : Level} {a : Set ℓ₁} {b : Set ℓ₂} → a → b
