module DH.Test.Inspection where

open import DH.Prelude
open import DH.Generics

open import DH.Data.Nat

import DH.Data.Type.Nat as N
import DH.Data.Fin as F
import DH.Data.Fin.Enum as E

open F

open import DH.Unsafe.Coerce

-------------------------------------------------------------------------------
-- InlineInduction
-------------------------------------------------------------------------------

-- | This doesn't evaluate compile time.
lhsInline : ℤ
lhsInline = unTagged (N.inlineInduction {n = 5} (λ n → Tagged n ℤ) (pure 0) (retag ∘ fmap succ))

-- | This doesn't evaluate compile time.
lhsNormal : ℤ
lhsNormal = unTagged (N.induction1 {n = 5} Tagged (pure 0) (retag ∘ fmap succ))

rhs : ℤ
rhs = 5

{-
inspect $ 'lhsInline === 'rhs
inspect $ 'lhsNormal =/= 'rhs
-}

inspect1 : lhsInline ≡ rhs
inspect1 = refl

-- interestingly, this works, too:
inspect2 : lhsNormal ≡ rhs
inspect2 = refl

-------------------------------------------------------------------------------
-- Enum
-------------------------------------------------------------------------------

-- | Note: GHC 8.0 (but not GHC 8.2?) seems to be
-- so smart, it reuses dictionary value.
--
-- Therefore, we define own local Ordering'
data Ordering' : Set where
  LT' EQ' GT' : Ordering'

RepIOrdering' : RepType
RepIOrdering' = U1 :+: U1 :+: U1

fromOrdering' : Ordering' → I RepIOrdering'
fromOrdering' LT' = Left tt
fromOrdering' EQ' = Right (Left tt)
fromOrdering' GT' = Right (Right tt)

toOrdering' : I RepIOrdering' → Ordering'
toOrdering' (Left tt) = LT'
toOrdering' (Right (Left tt)) = EQ'
toOrdering' (Right (Right tt)) = GT'

instance
  GenericOrdering' : Generic Ordering'
  GenericOrdering' = record { RepI = U1 :+: U1 :+: U1
                            ; from = fromOrdering'
                            ; to = toOrdering' }

lhsEnum : Ordering' → F.Fin 3
lhsEnum = E.gfrom

rhsEnum : Ordering' → F.Fin 3
rhsEnum LT' = FZ
rhsEnum EQ' = FS FZ
rhsEnum GT' = FS (FS FZ)

{-
inspect $ 'lhsEnum ==- 'rhsEnum
-}

-- Agda can't do this without case analysis
{-
inspect3 : lhsEnum ≡ rhsEnum
inspect3 = refl
-}

-------------------------------------------------------------------------------
-- Proofs
-------------------------------------------------------------------------------

lhsProof : ∀ {n} → F.Fin (n * 1) → F.Fin n
lhsProof {n = n} x rewrite N.proofMultNOne {n = n} = x

rhsProof : ∀ {n} → F.Fin (n * 1) → F.Fin n
rhsProof x = unsafeCoerce x
{-
inspect $ 'lhsProof ==- 'rhsProof
-}

-------------------------------------------------------------------------------
-- unfoldedFix
-------------------------------------------------------------------------------

foldrF : (a → b → b) → b → (List a → b) → List a → b
foldrF _f  z _go []       = z
foldrF  f _z  go (x ∷ xs) = f x (go xs)

superfold : List ℤ → ℤ
superfold = N.unfoldedFix 5 (foldrF _+_ 0)

-- Note: we need to write list explicitly, cannot use shorthand [1..4]
-- 'enumFromTo' is a recursive function!
--
-- Try to change [1,2,4,] to [1..4] to see the generated core :)
lhsFold : ℤ
lhsFold = superfold (1 ∷ 2 ∷ 3 ∷ 4 ∷ [])

lhsFold' : ℤ
lhsFold' = fix (foldrF _+_ 0) (1 ∷ 2 ∷ 3 ∷ 4 ∷ [])

rhsFold : ℤ
rhsFold = 10

{-
inspect $ 'lhsFold  === 'rhsFold
inspect $ 'lhsFold' =/= 'rhsFold
-}

lhsUnfold : ∀ {ℓ : Level} {a : Set ℓ} → (a → a) → a
lhsUnfold f = N.unfoldedFix 3 f

rhsUnfold : ∀ {ℓ : Level} {a : Set ℓ} → (a → a) → a
rhsUnfold f = f (f (f (fix f)))

{-
inspect $  'lhsUnfold === 'rhsUnfold
-}

inspect5 : ∀ {ℓ : Level} {a : Set ℓ} → lhsUnfold {a = a} ≡ rhsUnfold
inspect5 = refl

-------------------------------------------------------------------------------
-- Power
-------------------------------------------------------------------------------

{-
-- couldn't get this working because of level trouble
power : ∀ n → {{_ : N.InlineInduction n}} → ℤ → ℤ
power n k = unTagged impl where
    impl : Tagged n ℤ
    impl = N.inlineInduction (λ m → Tagged m ℤ) (MkTagged 1) $
           λ (MkTagged rec') → MkTagged (rec' * k)

lhsPower5 :: Int -> Int
lhsPower5 = power (Proxy :: Proxy N.Nat5)

rhsPower5 :: Int -> Int
rhsPower5 k = k * k * k * k * k

inspect $ 'lhsPower5 === 'rhsPower5
-}
